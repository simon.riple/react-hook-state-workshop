const express = require("express");
const app = express();
const port = 4000;

var players = [
  {
    playerId: 1,
    firstname: "Simon",
    lastname: "Riple",
    team: "Sotra Sportsklubb",
    score: 4,
  },
  {
    playerId: 2,
    firstname: "Cobe",
    lastname: "Bryant",
    team: "LA Lakers",
    score: 1,
  },
  {
    playerId: 3,
    firstname: "Tom",
    lastname: "Brady",
    team: "Tampa Bay Buccaneers",
    score: 2,
  },
];

const sensor = {
  Fuck: "Solskinn",
  Faen: "Regnbue",
  Helvette: "Regnbue",
  Dritt: "Stjernestøv",
};

const sanitize = (player) => ({
  playerId: player.playerId,
  firstname: sensor[player.firstname] ? sensor[player.firstname] : player.firstname,
  lastname: sensor[player.lastname] ? sensor[player.lastname] : player.lastname,
  team: sensor[player.team] ? sensor[player.team] : player.team,
  score: player.score < 0 ? 0 : player.score
});

app.use(express.json());

app.get("/api", (req, res) => {
  res.send("Hello World!");
});

app.get("/api/player/:playerId", (req, res) => {
  const player = players.find(
    (player) => player.playerId == req.params.playerId
  );
  if (player) {
    res.json(player);
  }
  res.json({ error: `Could not find player with id: ${req.params.playerId}` });
});

// app.post("/api/player", (req, res) => {
//   const newId = Math.floor(Math.random()*1000)
//   const newplayer = {...req.body.player, playerId: newId};
//   players = [...players, newplayer];
//   res.json(newplayer);
// });

app.put("/api/player/:playerId", (req, res) => {
  const sanitizedPlayer = sanitize(req.body.player);
  players = players.map((player) =>
    player.playerId == req.params.playerId ? sanitizedPlayer : player
  );
  res.json(sanitizedPlayer);
});

app.get("/api/players", (req, res) => {
  res.json(players);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
