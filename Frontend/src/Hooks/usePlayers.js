import { usePlayersStore } from "../State/PlayersStore";

export const usePlayers = () => {
  const { state, dispatch } = usePlayersStore();

  const refetchPlayers = () => {
      fetch('/api/players').then((res) => res.json()).then((players) => dispatch({type:"setPlayers", players}))
  }
  return {players: state.players, refetchPlayers}
};
