import { useState, useEffect } from "react";

export const useEntity = (emptyEntity, entityName, entityId, entityUrl) => {
  const [entity, setEntity] = useState(emptyEntity);

  useEffect(() => {
    fetch(`${entityUrl}${entityId}`)
      .then((res) => res.json())
      .then((entity) => setEntity(entity));
  }, [entityId, entityUrl]);

  const changeEntity = (name, value) =>setEntity({...entity, [name]: value});

  const putEntityOnServer = (callback) => {
      fetch(`${entityUrl}${entityId}`, {
        method: "put",
        body: JSON.stringify({ [entityName]: entity }),
        headers: {
          "Content-Type": "application/json",
        },
      }).then((res) => res.json())
      .then((entity) => {
        setEntity(entity);
        callback();
      });
  }

  return [entity, changeEntity, putEntityOnServer];
};
