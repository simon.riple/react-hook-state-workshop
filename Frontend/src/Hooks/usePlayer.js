import { useEntity } from "./useEntity";

export const usePlayer = (playerId) => {
  const [player, changePlayer, putPlayerOnServer] = useEntity(
    {
      playerId: "",
      firstname: "",
      lastname: "",
      team: "",
      score: "",
    },
    "player",
    playerId,
    "/api/player/"
  );

  return { player, changePlayer, putPlayerOnServer };
};
