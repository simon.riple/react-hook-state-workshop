import React, { createContext, useContext, useReducer, useEffect } from "react";

const PlayersContext = createContext();
const initialstate = { players: [] };

const reducer = (state, action) => {
  switch (action.type) {
    case "setPlayers":
      return { ...state, players: action.players };
    default:
      throw new Error("Unhandled action type: " + action.type);
  }
};

export const PlayersProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialstate);

    useEffect(() => {
        fetch("/api/players")
          .then((res) => res.json())
          .then((players) => dispatch({ type: "setPlayers", players }));
      }, []);
    
    return (
        <PlayersContext.Provider value={{state, dispatch}}>
            {children}
        </PlayersContext.Provider>
    )
}

export const usePlayersStore = () => useContext(PlayersContext);
