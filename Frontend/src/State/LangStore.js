import React, { createContext, useContext, useReducer } from "react";

const LangContext = createContext();
const initialstate = { lang: undefined };

const reducer = (state, action) => {
  switch (action.type) {
    case "switchLanguage":
      return { ...state, lang: action.lang };
    default:
      throw new Error("Unhandled action type: " + action.type);
  }
};

export const LangProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialstate);

    return (
        <LangContext.Provider value={{state, dispatch}}>
            {children}
        </LangContext.Provider>
    )
}

export const useLang = () => useContext(LangContext);
