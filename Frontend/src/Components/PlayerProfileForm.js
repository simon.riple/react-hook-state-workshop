import React, { useState, useEffect } from "react";
import { usePlayers } from "../Hooks/usePlayers";
import {usePlayer} from "../Hooks/usePlayer";
import { Text } from "./Text";

export const PlayerProfileForm = (props) => {
  const { refetchPlayers} = usePlayers();
  const {player, changePlayer, putPlayerOnServer} = usePlayer(props.playerId)

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    changePlayer(name,value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    putPlayerOnServer(refetchPlayers)
  };

  return (
    <div>
      <div>
        <button onClick={props.removeSelectedPlayer}>
          <Text id={"close"}>Close</Text>
        </button>
      </div>
      <form onSubmit={handleSubmit}>
        <label>
          <Text className={"Input-label"} id={"firstname"}>
            Firstname:
          </Text>
          <input
            type="text"
            name="firstname"
            value={player.firstname}
            onChange={handleChange}
          />
        </label>
        <label>
          <Text className={"Input-label"} id={"lastname"}>
            Lastname:
          </Text>
          <input
            type="text"
            name="lastname"
            value={player.lastname}
            onChange={handleChange}
          />
        </label>
        <label>
          <Text className={"Input-label"} id={"team"}>
            Team:
          </Text>
          <input
            type="text"
            name="team"
            value={player.team}
            onChange={handleChange}
          />
        </label>
        <label>
          <Text className={"Input-label"} id={"score"}>
            Score:
          </Text>
          <input
            type="text"
            name="score"
            value={player.score}
            onChange={handleChange}
          />
        </label>
        <button type="submit">
          <Text id={"submit"}>Save</Text>
        </button>
      </form>
    </div>
  );
};
