import React, { useEffect, useState } from "react";
import { usePlayers } from "../Hooks/usePlayers";
import {Text} from "./Text";
export const TopScorersList = () => {
  const {players} = usePlayers();

  return (
    <div>
      <Text id={"topScorers"}>Top scorers:</Text>
      <ul>
        {players.sort((a,b) => b.score - a.score).map((player) => (
          <li>{player.lastname + " - " + player.score}</li>
        ))}
      </ul>
    </div>
  );
};
