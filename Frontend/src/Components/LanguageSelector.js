import React from "react";
import { useLang } from "../State/LangStore";
import { Text } from "./Text";

export const LanguageSelector = () => {
  const { state, dispatch } = useLang();
  return (
    <div className={"LanguageSelector"}>
      <ul>
        <li>
          <button
            style={{
              backgroundColor: state.lang === "no" ? "grey" : "white",
            }}
            onClick={() => dispatch({ type: "switchLanguage", lang: "no" })}
          >
            <Text>Norwegian</Text>
          </button>
        </li>
        <li>
          <button
          style={{
            backgroundColor: state.lang === "es" ? "grey" : "white",
          }}
            onClick={() => dispatch({ type: "switchLanguage", lang: "es" })}
          >
            <Text>Spanish</Text>
          </button>
        </li>
        <li>
          <button
          style={{
            backgroundColor: state.lang === undefined ? "grey" : "white",
          }}
            onClick={() =>
              dispatch({ type: "switchLanguage", lang: undefined })
            }
          >
            <Text>English</Text>
          </button>
        </li>
      </ul>
    </div>
  );
};
