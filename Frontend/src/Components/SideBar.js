import React from "react";
import sportsLogo from "../sports.png";
import { TopScorersList } from "./TopPlayersList";
import {Text} from "./Text";
import {LanguageSelector} from "./LanguageSelector";

export const SideBar = () => {
  
  return (
    <div className="SideBar">
      <img src={sportsLogo} width={"60%"} alt={"sports logo"} />
      <Text variant={"h1"} className={"SideBar-header"} id={"sports"}>Sports</Text>
      <TopScorersList />
      <LanguageSelector />
    </div>
  );
};
