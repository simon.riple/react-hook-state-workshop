import React from "react";
import { useLang } from "../State/LangStore";
import { Translations } from "../Translations";

const getTranslatedString = (lang, id) => {
  const string = Translations?.[lang]?.[id];
  return string;
};
export const Text = (props) => {
  const { variant, id, className, children } = props;

  const { state } = useLang();

  switch (variant) {
    case "h1":
      return (
        <h1 className={className}>
          {getTranslatedString(state.lang, id) || children}
        </h1>
      );
    default:
      return (
        <p className={className}>
          {getTranslatedString(state.lang, id) || children}
        </p>
      );
  }
};
