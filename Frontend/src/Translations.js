export const Translations = {
    no: {
      firstname: "Fornavn",
      lastname: "Etternavn",
      team: "Lag",
      score: "Poeng",
      submit: "Lagre",
      close: "Lukk",
      sports: "Sports",
      topScorers: "Toppscorere",
      playerProfilepage: "Spiller profil side",
      playersPage: "Spillere side",
    },
    es: {
      firstname: "Nombre",
      lastname: "Apellido",
      team: "Equipo",
      score: "Puntos",
      submit: "Salvar",
      close: "Cerrar",
      sports: "Deportes",
      topScorers: "Mejores goleadores",
      playerProfilepage: "Página de perfil de jugador",
      playersPage: "Página de jugadores",
    }
  };