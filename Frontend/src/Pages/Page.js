import React from "react";
import { PlayersPage } from "./PlayersPage";

export const Page = () => {
  return (
    <div className="Page">
      <PlayersPage />
    </div>
  );
};
