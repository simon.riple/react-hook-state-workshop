import React, { useState, useEffect } from "react";
import { PlayerProfilePage } from "./PlayerProfilePage";
import { Text } from "../Components/Text";
import { usePlayers } from "../Hooks/usePlayers";

export const PlayersPage = (props) => {
  const {players} = usePlayers();
  const [selectedPlayer, setSelectedPlayer] = useState(null);

  const onSelectPlayer = (playerId) => {
    setSelectedPlayer(playerId);
  };

  return (
    <>
      <Text variant={"h1"} id={"playersPage"}>
        Players page
      </Text>
      {selectedPlayer === null ? (
        <ul>
          {players.map((player) => (
            <li onClick={() => onSelectPlayer(player.playerId)}>
              <p>{player.firstname + " " + player.lastname}</p>
            </li>
          ))}
        </ul>
      ) : (
        <PlayerProfilePage
          playerId={selectedPlayer}
          removeSelectedPlayer={() => onSelectPlayer(null)}
        />
      )}
    </>
  );
};
