import React from "react";
import { PlayerProfileForm } from "../Components/PlayerProfileForm";
import {Text} from "../Components/Text";

export const PlayerProfilePage = (props) => {
  const { playerId, removeSelectedPlayer } = props;

  return (
    <>
      <Text variant={"h1"} id={"playerProfilePage"}>Player profile page</Text>
      <PlayerProfileForm
        playerId={playerId}
        removeSelectedPlayer={removeSelectedPlayer}
      />
    </>
  );
};
