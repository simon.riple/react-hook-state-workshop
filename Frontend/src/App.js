import React, { useEffect, useState } from "react";
import "./App.css";
import { SideBar } from "./Components/SideBar";
import { Page } from "./Pages/Page";
import { LangProvider } from "./State/LangStore";
import { PlayersProvider } from "./State/PlayersStore";

export const App = () => {
  
  return (
    <div className="App">
      <LangProvider>
        <PlayersProvider>
          <SideBar />
          <Page />
        </PlayersProvider>
      </LangProvider>
    </div>
  );
};

export default App;
